package com.janeschitz.codekata.kata09;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author cjaneschitz
 */
@RunWith(JUnit4.class)
public class CheckOutTest {


    /*

      Item   Unit      Special
             Price     Price
      --------------------------
        A     50       3 for 130
        B     30       2 for 45
        C     20
        D     15

     */

    @Test
    public void deveCalcularValorFinalConsiderandoDescontosComBaseNaTabelaFornecida() {

        CheckOut checkOut = new CheckOut(new TabelaDescontosPadrao());

        checkOut
            .scan("A")
            .scan("A")
            .scan("A")
            .scan("A")
            .scan("B")
            .scan("B")
            .scan("C")
        ;

        assertEquals(245, checkOut.getTotal());

    }

    @Test
    public void deveCalcularValorFinalConsiderandoDescontosComBaseEmUmaOutraTabelaFornecida() {

        CheckOut checkOut = new CheckOut(new ITabelaDescontos() {
            @Override
            public long getValorDesconto(Map.Entry<String, Integer> item) {
                return -1 * item.getValue();
            }
        });

        checkOut
                .scan("A")
                .scan("A")
                .scan("A")
                .scan("A")
                .scan("B")
                .scan("B")
                .scan("C")
        ;

        assertEquals(273, checkOut.getTotal());

    }

    @Test
    public void deveCalcularValorFinalSemDescontosSeForFornecidaTabelaDeDescontosNula() {

        CheckOut checkOut = new CheckOut(null);

        checkOut
                .scan("A")
                .scan("A")
                .scan("A")
                .scan("A")
                .scan("B")
                .scan("B")
                .scan("C")
        ;

        assertEquals(280, checkOut.getTotal());

    }

}
