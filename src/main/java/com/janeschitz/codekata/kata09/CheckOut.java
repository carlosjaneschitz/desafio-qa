package com.janeschitz.codekata.kata09;

import java.util.HashMap;
import java.util.Map;

/**
 * Armazena itens e calcula o valor total
 * @author cjaneschitz
 */
public class CheckOut {

    /**
     * Estratégia definida para cálculo de descontos
     */
    private ITabelaDescontos tabelaDescontos;

    /**
     * Armazena quais itens foram adicionados e em qual quantidade
     */
    private Map<String, Integer> itens = new HashMap<>();

    public CheckOut(ITabelaDescontos tabelaDescontos) {
        this.tabelaDescontos = tabelaDescontos;
    }

    /**
     * Calcula o valor total de todos os itens adicionados já com os possíveis descontos aplicados
     * @return o valor total
     */
    public long getTotal() {

        long total = 0; // valor total

        // para cada item armazenado...
        for (Map.Entry<String, Integer> item : itens.entrySet()) {

            TabelaPrecosUnitarios itemTabela = TabelaPrecosUnitarios.valueOf(item.getKey());    // FIXME não considerado um
                                                                                                // item inexistente

            long valorUnitario = itemTabela.getPrecoUnitario();
            int quantidade = item.getValue();

            total += valorUnitario * quantidade; // cálculo padrão: quantidade * preço unitário
            total += tabelaDescontos != null ? tabelaDescontos.getValorDesconto(item) : 0;  // se foi fornecida uma estratégia
                                                                                            // de descontos, recupera o valor
                                                                                            // de desconto equivalente ao item
                                                                                            // (se houver)

        }

        return total;
    }

    /**
     * Adiciona um item para totalização
     * @param item o item a ser adicionado
     * @return a instância de Checkout (syntax suggar)
     */
    public CheckOut scan(String item) {

        if (!itens.containsKey(item)) {
            itens.put(item, 0);
        }

        itens.put(item, itens.get(item) + 1);

        return this;
    }
}