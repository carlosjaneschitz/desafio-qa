package com.janeschitz.codekata.kata09;

import java.util.Map;

/**
 * Contrato para estratégias de descontos
 * @author cjaneschitz
 */
public interface ITabelaDescontos {
    long getValorDesconto(Map.Entry<String, Integer> item);
}
