package com.janeschitz.codekata.kata09;

/**
 * Representação de uma tabela de preços unitários de itens
 * @author cjaneschitz
 */
public enum TabelaPrecosUnitarios {

    A(50),
    B(30),
    C(20),
    D(15);

    private long precoUnitario;

    TabelaPrecosUnitarios(long precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    long getPrecoUnitario() {
        return this.precoUnitario;
    }

}
