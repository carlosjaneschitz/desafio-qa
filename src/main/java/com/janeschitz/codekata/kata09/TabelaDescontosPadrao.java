package com.janeschitz.codekata.kata09;

import java.util.Map;

/**
 * Uma implementação de estratégia de tabela de descontos
 * @author cjaneschitz
 */
public class TabelaDescontosPadrao implements ITabelaDescontos {

    @Override
    public long getValorDesconto(Map.Entry<String, Integer> item) {

        long desconto;

        switch (item.getKey()) {
            case "A": {
                desconto = -20 * (item.getValue() / 3);
                break;
            }
            case "B": {
                desconto = -15 * (item.getValue() / 2);
                break;
            }
            default: {
                desconto = 0;
            }
        }

        return desconto;
    }
}